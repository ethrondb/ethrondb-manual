# Introducción a EthronDB

**EthronDB** es un *driver* que permite la tokenización en redes Ethereum.

Características:

- Utiliza un lenguaje de consulta similar a SQL para acceder a los datos.
  El trabajo sucio de Web3.js lo hace el *driver*.
- Es fácil de aprender y de usar.

Tecnologías usadas:

- Lenguajes de programación: [Dogma](http://dogmalang.com) (compilado a JavaScript) y [Solidity](https://solidity.readthedocs.io/en/latest/).
- Automatización de tareas: Ethron.js.
- Automatización de pruebas de unidad: Ethron.js.
- Docker.
- Implementaciones: Geth, Parity y Ganache.

# Bases de datos

Una **base de datos** (*database*) es un contenedor de datos estructurado en almacenes.
Donde cada almacén es similar a un contenedor de *Cosmos DB*, colección de *MongoDB* o tabla en *SQL*.

## Creación de base de datos

Para crear una base de datos, hay que utilizar la operación `CREATE DATABASE`:

```
CREATE DATABASE nombre
```

La operación devuelve la dirección de Ethereum donde se encuentra el contrato que representa la base de datos.
Para abrir una conexión contra la base de datos, hay que utilizar la dirección devuelta.
Ejemplo:

```
//imports
import EthronDB from "@ethrondb/client";

//Apertura de conexión:
const endpoint = "...";
const addr = "dirección";
const cli = new EthronDB({endpoint, db: addr});
await(cli.connect());
```

## Supresión de base de datos

Para suprimir la base de datos actual:

```
DROP DATABASE
```

# Elementos clave-valor

Un **elemento clave-valor** (*key-value item*) es la unidad de almacenamiento más pequeña dentro de un almacén clave-valor.
Representa los elementos de datos, representados por pares clave-valor.
Donde la **clave** (*key*) es el identificador único dentro del almacén.
Y el **valor** (*value*), su dato almacenado, el cual se almacenará en texto formateado como JSON.

## Propiedades de los elementos clave-valor

Todo elemento clave-valor debe tener un campo `key`, el cual se utiliza como clave.
Si no se indica, se usará como clave un número de secuencia generado automáticamente por el almacén.
Esto último sólo si el almacén tiene su propiedad `auto` a `true`.

## Inserción y actualización de elementos

Para insertar un nuevo elemento clave-valor, usar la operación `INSERT`:

```
INSERT {
  campo1: valor
  campo2: valor
  ...
}
INTO Almacén
WITH {
  opción1: valor
  opción2: valor
  ...
}
```

Ejemplo:

```
INSERT {
  key: "the scream"
  year: 1893
  author: "Edvard Munch"
  price: 120000000
}
INTO Pictures
```

Si lo que deseamos es reemplazar el valor de un elemento por otro:

```
SET {
  ...
}
INTO Almacén
```

Otro ejemplo:

```
INSERT {
  key: "ticket123"
  price: 30
}
INTO events.EchoAndTheBunnymen20190710
```

## Selección de elementos

Para obtener el valor de una clave, usar la operación `SELECT`:

```
#un único elemento
SELECT FROM Almacén
WHERE key == "clave"

#todos los elementos
SELECT FROM Almacén
```

Ejemplo:

```
SELECT FROM Pictures
WHERE key == "the scream"
```

## Supresión de elementos

Para suprimir un elemento, usar la operación `DELETE`:

```
DELETE FROM Almacén
WHERE key == "clave"
```

Para suprimir todos los elementos, usar `TRUNCATE`:

```
TRUNCATE Almacén
```

## Propiedad de los elementos

Todo elemento tiene un **propietario** (*owner*), aquel que tiene su propiedad.
El usuario que inserta el elemento se convierte en su propietario.
El concepto de propiedad tiene dos objetivos.
Por un lado, reflejar quién es su propietario actual.
Y por otro lado, permitirle al usuario modificar el valor.
Sólo el usuario propietario puede modificar el valor.
Los demás pueden leerlo, pero no así modificarlo.

Por otro lado, se utiliza el concepto de **licencia** (*license*), el permiso para ser propietario sin poder modificar el valor.

### Transferencia de propiedad

Para transferir la propiedad de un elemento a otro usuario, el propietario actual debe usar la operación `TRANSFER OWNERSHIP`:

```
TRANSFER OWNERSHIP TO usuario
FROM Almacén
WHERE key == "clave"
```

Ejemplos:

```
TRANSFER OWNERSHIP TO b1ccdb544f603af631525ec406245909ad6e1b60
FROM Pictures
WHERE key == "the scream"

TRANSFER OWNERSHIP TO b1ccdb544f603af631525ec406245909ad6e1b60
FROM events.EchoAndTheBunnymen20190710
WHERE key == "ticket123"
```

Si el almacén se ha configurado con `transfer` a `accept`, la transferencia es incondicional.
En cambio, si se configura como `approve`, el receptor debe aceptar la transferencia.
Mientras el receptor no apruebe la transferencia, el propietario actual puede retransferirlo a otro con `TRANSFER OWNERSHIP`.
O bien, cancelar la transferencia con `CANCEL OWNERSHIP TRANSFER`:

```
CANCEL OWNERSHIP TRANSFER
FROM Almacén
WHERE key == "clave"
```

Para aceptar una transferencia, el receptor debe usar:

```
APPROVE OWNERSHIP TRANSFER
FROM Almacén
WHERE key == "clave"
```

Para conocer las aprobaciones que el usuario actual tiene pendientes:

```
PENDING FROM Almacén
```

### Ruta de transferencias

En el momento de insertar un nuevo elemento, se puede fijar la posible ruta de transferencia de propiedad.
La **ruta de transferencia** (*transfer path*) fija la ruta de propiedad que hay que seguir.
Esto permite controlar a quiénes se les puede transferir la propiedad y cuándo.

Esta ruta es un array, el cual contiene textos con alguno de los siguientes formatos:

- `*`, representa una cuenta cualquiera.
- `**`, representa una secuencia infinita de cuentas cualquiera.
- `dirección`, representa la dirección de una determinada cuenta.

P. ej., supongamos un sistema de entradas.
Inicialmente, el primer receptor de la propiedad podría ser el promotor del concierto.
El segundo la entidad que vende la entrada.
A continuación, la persona que adquiere la entrada.
Después el local donde se realiza el concierto.
Y finalmente, el promotor otra vez.
¿Cómo se indicaría esto? Fácil: `["dirPromotor", "dirVentaEntradas", "*", "dirLocal", "dirPromotor"]`.
Donde dirPromotor, dirVentaEntradas y dirLocal deberían ser las direcciones de las cuentas correspondientes.
Al hacerlo así, el comprador no podrá revender su entrada.

Ahora supongamos que permitimos que la entrada, antes de transferírsela al local, pueda pasar por infinitas manos:
`["dirPromotor", "dirVentaEntradas", "**", "dirLocal", "dirPromotor"]`.

Cada vez que se realiza una transferencia, la ruta de transferencia se va reduciendo.
De tal manera que:

- Si la ruta se queda vacía, ya no se podrá transferir más su propiedad.
- Si se alcanza `**` en un punto de la ruta, se mantiene ésta hasta que una transferencia sea la siguiente de la ruta.
- No se permitirá una transferencia si su ruta no lo permite.

Para fijar la ruta de transferencia, recordemos sólo se puede fijar en la inserción, se usa la cláusula `with`:

```
INSERT {
  key: "ticket123"
  price: 43.50
  area: "lawn"
}
INTO events.TheNational20190611
WITH {
  transfer: ["vendedor", "*", "lugar", "promotor"]
}
```

Recordemos que *vendedor*, *lugar* y *promotor* son las direcciones de Ethereum de las cuentas correspondientes.

El problema es que si transferimos la propiedad, su propietario podría modificar el valor.
Salvo la clave, todo se puede modificar.
Obviamente esto no es útil en todas las ocasiones y para resolver este tipo de problemas se usa la licencia.

### Licenciamiento

Mediante un licenciamiento, lo que se busca es representar una propiedad restringida, sin derecho a modificar el elemento.
Cuando un elemento está licenciado a otra cuenta, su propietario real no puede modificar el valor a menos que sea el propietario de nuevo.
Vamos a explicarnos.

Si *A* licencia o cede un elemento a *B*, *A* sigue siendo la única cuenta que puede modificar el valor.
*B* actuará como propietario, pero no podrá modificar el valor.
Y *A* no podrá modificar el valor mientras la propiedad la tenga *B*.

En el caso de un sistema de entradas, es más útil el licenciamiento que la transferencia de propiedad.
Así los compradores no podrán modificar los datos de la entrada.

En estos casos, cuando se define la ruta de transferencia, se puede indicar en cada salto que se trata de una licencia.
Para estos casos, el paso en la ruta de transferencia debe seguir alguno de los siguientes formatos:

```
license://dirección
license://dirección:timestamp
```

Si se indica una fecha, si el propietario licenciado no ha transferido la propiedad al siguiente, el propietario real podrá recuperar la propiedad o transferirla al siguiente.

He aquí unos ejemplos ilustrativos:

```
license://b1ccdb544f603af631525ec406245909ad6e1b60
license://b1ccdb544f603af631525ec406245909ad6e1b60:1582934399
```

Así pues, si el propietario real desea pasar la propiedad a la siguiente dirección de la ruta de transferencia, no tiene más que ejecutar:

```
TRANSFER OWNERSHIP TO dirección
FROM Almacén
WHERE key == "clave"
```

Si lo que desea es recuperar la propiedad:

```
TAKE OWNERSHIP
FROM Almacén
WHERE key == "clave"
```

### Uso de NEXT

Si se desea, se puede indicar que se transfiera la propiedad a la siguiente dirección de la ruta usando `NEXT` como dirección.
Ejemplo:

```
TRASNFER OWNERSHIP TO NEXT
FROM Almacén
WHERE key == "clave"
```

### Omisión de ruta

Cuando no se indica la opción `transfer` en un `INSERT`, el elemento tiene la siguiente ruta predeterminada: `["**"]`.
Lo que permite su cambio de propiedad sin límites.

## Parámetros de consulta

El *driver* soporta **parámetros** (*parameters*), un valor que se pasa en el momento de solicitar la ejecución de la operación.
Estos parámetros se representan mediante el siguiente formato: `&parámetro`.
He aquí un ejemplo ilustrativo:

```
INSERT &pic
INTO Pictures
WITH &opts
```

Ejemplo de llamada en JavaScript:

```
const pic = {...};
const opts = {...};
const resp = await(cli.exec("INSERT &pic INTO pictures WITH &opts", {pic, opts}));
```

Las consultas que presentan parámetros se conocen formalmente como **consultas parametrizadas** (*parameterized queries*).

En las condiciones, se puede indicar un parámetro como condición.
Ejemplo:

```
const resp = await(cli.exec("SELECT FROM pictures WHERE &key", {key: "the scream"}));

#lo mismos que:
const resp = await(cli.exec("SELECT FROM pictures WHERE key == &key", {key: "the scream"}));
```

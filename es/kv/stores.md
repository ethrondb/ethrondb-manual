# Almacenes de pares clave-valor

Un **almacén** (*store*) no es más que un contenedor de elementos o ítems.
Cada **elemento** (*item*) representa un documento de datos, representado por un par clave-valor.
Donde la **clave** (*key*) representa el identificador único dentro del almacén.
Y el **valor** (*value*), su dato almacenado, el cual se almacenará en formato texto.

Todo almacén tiene un nombre, a través del cual identificarlo del resto.
Y se representa mediante su propio contrato.

## Creación de almacenes

Para crear un almacén clave-valor, hay que utilizar la operación `CREATE STORE`.
Ejemplo:

```
CREATE STORE Almacén
WITH {
  opción1: valor
  opción2: valor
  ...
}
```

Opciones o propiedades disponibles:

- `type` (texto, requerido): tipo del almacén. Debe ser siempre: `kv`.
- `max` (número): número máximo de elementos que pueden insertarse en el almacén.
- `desc` (texto): descripción del almacén.
- `extra` (objeto): info adicional del creador del almacén.
- `auto` (booleano): ¿el almacén puede generar el valor de las claves de los elementos si no se indican explicitamente?
- `transfer` (texto): ¿configuración de transferencia de propiedad? `accept` o `approve`.

Ejemplo:

```
CREATE STORE events.EchoAndTheBunnymen20190710
WITH {
  type: "kv"
  max: 1250
  desc: "Concierto de Echo & the Bunnymen, 10 julio 2019 en Valencia"
  extra: {
    venue: "Jardines de Viveros"
    town: "Valencia, Spain"
    date: "2019-07-10"
  }
}
```

Cada almacén se representa mediante su propio contrato.
Y su dirección se almacena en el contrato de la base de datos.

## Propiedades de los almacenes

### Nombre del almacén

Todo almacén debe tener un identificador único dentro de la base de datos.
El cual debe seguir uno de los siguientes formatos:

```
[a-zA-Z][a-zA-Z0-9]+
[a-zA-Z][a-zA-Z0-9]+\.[a-zA-Z][a-zA-Z0-9]+
```

### Tipo del almacén

**EthronDB** puede trabajar con distintos tipos de almacenes.
Para indicar un almacén de tipo clave-valor, su tipo, indicado por el campo `type`, debe ser `kv`.

### Contador de elementos

El **contador de elementos** (*item counter*) indica el número de elementos insertados en el almacén.
Cada vez que se inserta un elemento, se incrementa automáticamente este contador.

Si lo deseamos, podemos fijar el número máximo de elementos que se puede insertar.
Alcanzado éste, ya no se podrá insertar más elementos, ni aun si borramos existentes.
Para ello, se puede utilizar el campo `max` en la creación del almacén.
Si no se indica explícitamente, se podrá insertar infinitos elementos.

Por ejemplo, supongamos que hemos creado un sistema de entradas para conciertos.
Si el evento se desarrolla, como parece natural, en un recinto con aforo limitado, se puede indicar el número total de entradas a emitir.
Una vez creado el almacén para el evento, el promotor no podrá generar más elementos que los limitados por su aforo.

### Propietario del almacén

El **propietario del almacén** (*store owner*) es la cuenta de Ethereum que lo creó.
No se puede cambiar ni fijar otro.

### Descripción del almacén

A través de la propiedad `desc`, se puede indicar el objeto del almacén.

### Info adicional

Mediante la propiedad `extra`, se puede proporcionar información adicional a la descripción.
Es un objeto JSON.

### Clave automática

Cuando se fija `auto` a `true`, el almacén puede generar el valor de la clave de los nuevos elementos.
Si no se indica, al insertarse un nuevo ítem, éste deberá proporcionar su propia clave.

## Supresión de almacenes

Para suprimir un almacén, utilizar la operación `DROP STORE`:

```
DROP STORE Almacén
```

Ejemplo:

```
DROP STORE events.EchoAndTheBunnymen20190710
```

## Listado de almacenes

Para obtener la lista de almacenes, usar la operación `SELECT`:

```
#todos
SELECT FROM db.Stores

#uno específico
SELECT FROM db.Stores
WHERE key == "nombre almacén"
```

Ejemplo:

```
SELECT FROM db.Stores
WHERE key == "events.EchoAndTheBunnymen20190710"
```

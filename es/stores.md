# Almacenes

Un **almacén** (*store*) no es más que un contenedor de ítems.
Donde cada **ítem** (*item*) representa un elemento de datos.

Actualmente, se encuentran disponibles los siguientes tipos de almacén:

- Almacenes clave-valor, que permiten almacenar pares clave-valor.

En el futuro, se añadirán más tipos de almacenes.
